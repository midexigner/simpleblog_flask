# Create a Blog
### Flask Folder Structure

```sh
projectName/
├── static/
│   ├── css
│   ├── fonts
│   ├── img
│   └── js
├── templates/
│   ├── header.py
│   └── footer.py
└── /
    ├── app.py
    ├── blog.db
    
```
### Setup
...
- pip install flask
- pip install flask
- create abc.db
- python
- from app import db
- db.create_all()

- https://stackoverflow.com/questions/19260067/sqlalchemy-engine-absolute-path-url-in-windows

### Resources
- https://www.youtube.com/watch?v=DD3ou9sa3Z8&list=PLu0W_9lII9agAiWp6Y41ueUKx1VcTRxmf ( Web Development Using Flask and Python)
- https://www.youtube.com/channel/UC-QDfvrRIDB6F0bIO4I4HkQ/playlists