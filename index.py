from flask import Flask, request, flash, url_for, redirect, render_template
from flask_bootstrap import Bootstrap
from bs4 import BeautifulSoup
import requests

app = Flask(__name__)

bootstrap = Bootstrap(app)

base_url = "https://www.imdb.com/chart/top?ref_=nv_mv_250"
r=requests.get(base_url)
soup = BeautifulSoup(r.text,"html.parser")
products = soup.find('table', {'class': 'chart full-width'})
product_detail = products.find("tbody")
rows = product_detail.find_all('tr')

#print(products)
l1 = []
for item in rows:
    d={}
    product_description = item.find('td',{'class':'titleColumn'})
    product_description = product_description.text.replace('\n', "").strip()
   # moviename = moviename.text.replace('\n', '').strip()
    image =item.find('td',{'class':'posterColumn'})
    image = image.find("img")
    image = image.get("src")
    d['name'] = product_description
    d['img'] = image
    l1.append(d)
#print(l1)

@app.route('/')
def index():
    return render_template('index.html',data=l1)

if __name__ == '__main__':
   app.run(debug = True)
   #app.run(port=1000)